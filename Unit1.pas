﻿unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, ImgList;

type
  TMainFrame = class(TForm)
    Fon: TImage;
    RandBut: TButton;
    Marker: TImage;
    ExtBut: TButton;
    DeSelAllBut: TButton;
    SelAllBut: TButton;
    ResSelBut: TButton;
    ResBut: TButton;
    CheckBox: TCheckBox;
    Dop: TImage;
    Label1: TLabel;
    Label2: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure MyClick(Sender: TObject);
    procedure MyDblClick(Sender: TObject);
    procedure RandButClick(Sender: TObject);
    procedure ExtButClick(Sender: TObject);
    procedure ResSelButClick(Sender: TObject);
    procedure ResButClick(Sender: TObject);
    procedure SelAllButClick(Sender: TObject);
    procedure DeSelAllButClick(Sender: TObject);
    procedure DopMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DopMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DopMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
  private
    procedure MyUpdate(i:byte);
  public
    { Public declarations }
  end;

var
  MainFrame: TMainFrame;
  sel,stat,max,Amax: byte;
  ext,drag:boolean;
  pupil: array[1..255] of record
    obj:Timage;
    x,y:integer;
    state,selected:boolean;
  end;

implementation

{$R *.dfm}

//------------------------------//
procedure Run(j:byte);
var
  i:byte;
begin
  for i:=1 to j do
    if ((pupil[i].state) and (not pupil[i].selected)) then
      begin
        MainFrame.Marker.Left := pupil[i].x-3;
        MainFrame.Marker.Top := pupil[i].y-3;
        MainFrame.Marker.Visible := true;
        sleep(100);
        Application.ProcessMessages;
      end;
end;

//------------------------------//
procedure MyReset;
var
  i: byte;
begin
  for i := 1 to max do
    if (pupil[i].state) then
      begin
      pupil[i].selected := false;
      MainFrame.MyUpdate(i);
      end;
  sel := 0;
  MainFrame.Marker.visible := false;
  Application.ProcessMessages;
  sleep(200);
end;

//------------------------------//
procedure MyResSel;
var
  i: byte;
begin
  for i := 1 to max do
    if pupil[i].selected then
      begin
        pupil[i].selected := false;
        dec(sel);
        MainFrame.MyUpdate(i);
      end;
end;

//------------------------------//
procedure TMainFrame.MyUpdate(i:byte);
var
  st,sl:boolean;
begin
  st := pupil[i].state;
  sl := pupil[i].selected;
  with (Timage(pupil[i].obj)) do
    if st and sl then Picture.LoadFromFile('All.bmp')
      else if st then Picture.LoadFromFile('Stated.bmp')
        else if sl then Picture.LoadFromFile('Selected.bmp')
          else Picture := nil;
end;

//------------------------------//
procedure TMainFrame.MyClick(Sender: TObject);
var
  s:string;
  i:byte;
begin
s := Timage(Sender).Name;
i := StrToInt(copy(s,4,length(s)-3));

if pupil[i].state then
  if pupil[i].selected then dec(sel)
else else
  if pupil[i].selected then inc(sel);

pupil[i].state := not pupil[i].state;
if pupil[i].state then inc(stat) else dec(stat);
MyUpdate(i);
end;

//------------------------------//
procedure TMainFrame.MyDblClick(Sender: TObject);
var
  s:string;
  i:byte;
begin
  s := Timage(Sender).Name;
  i := StrToInt(copy(s,4,length(s)-3));
  if pupil[i].state then dec(stat);
  if pupil[i].selected then dec(sel);
  TImage(Sender).Free;
  dec(max);
end;

//------------------------------//
procedure TMainFrame.RandButClick(Sender: TObject);
var
  j,i,n:byte;
begin
  if not CheckBox.Checked then MyResSel;
  RandBut.Enabled := false;
  if (stat <> 0) then
  begin
    if (stat=sel) then MyReset;
    repeat
      n:=random(max)+1;
    until ((pupil[n].state) and (not pupil[n].selected));
    for j:=1 to random(3)+1 do
      run(max);
    run(n);
    pupil[n].selected:=true; inc(sel);
    MyUpdate(n);
  end
  else ShowMessage('Ничего не выбрано');
  RandBut.Enabled := true;
end;

//------------------------------//
procedure TMainFrame.ResSelButClick(Sender: TObject);
begin
  MyResSel;
end;

//------------------------------//
procedure TMainFrame.ResButClick(Sender: TObject);
var
  i:byte;
begin
  for i := Amax+1 to max do
    Timage(pupil[i].obj).Free;
  max := Amax;
  MainFrame.Marker.Visible := false;
  for i := 1 to max do
    begin
    pupil[i].state := false;
    pupil[i].selected := false;
    MainFrame.MyUpdate(i);
    end;
  Sel:=0; Stat:=0;
end;

//------------------------------//
procedure TMainFrame.SelAllButClick(Sender: TObject);
var
  i:byte;
begin
  for i := 1 to max do
    begin
    pupil[i].state := true;
    MainFrame.MyUpdate(i);
    end;
  stat:=max;
end;

//------------------------------//
procedure TMainFrame.DeSelAllButClick(Sender: TObject);
var
  i:byte;
begin
  for i := 1 to max do
    begin
    pupil[i].state := false;
    MainFrame.MyUpdate(i);
    end;
  Sel:=0;
end;

//------------------------------//
procedure TMainFrame.DopMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  //ShowMessage('Clac');
  drag := true;
end;

//------------------------------//
procedure TMainFrame.DopMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  if drag then
    with Dop do
      begin
        left:=mouse.CursorPos.X-MainFrame.Left-33;
        top:=mouse.CursorPos.y-MainFrame.Top-56;
      end;

end;

//------------------------------//
procedure TMainFrame.DopMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  drag := false;
  if Dop.Left<380 then
    begin
      inc(max);
      pupil[max].state := true;
      pupil[max].selected := false;
      pupil[max].obj := TImage.Create(MainFrame);
      pupil[max].x := Dop.Left+3;
      pupil[max].y := Dop.Top+3;
        with (pupil[max].obj) do
          begin
            Name := 'Pup' + IntToStr(max);
            Height := 26;
            Width := 26;
            Parent := MainFrame;
            Left := pupil[max].x-3;
            Top := pupil[max].y-3;
            Transparent := true;
            OnClick := MyClick;
            OnDblClick := MyDblClick;
          end;
      inc(stat);
      MyUpdate(max);
      end;
  with Dop do
    begin
      left:=490;
      top:=170;
    end;
end;

//------------------------------//
procedure TMainFrame.ExtButClick(Sender: TObject);
begin
  if ext then
    begin
      MainFrame.ClientWidth := 425;
      ExtBut.Caption := '→';
    end
  else
    begin
      MainFrame.ClientWidth := 525;
      ExtBut.Caption := '←';
    end;
  ext := not ext;
end;

//------------------------------//
procedure TMainFrame.FormCreate(Sender: TObject);
var
  i: byte;
begin
  Randomize;
  ext:=false; drag:=false;
  MainFrame.ClientWidth := 425;
  sel := 0; stat := 0;
  AssignFile(input,'Cord.txt');
  Reset(input);
  Readln(max);
  Amax := max;
  for i := 1 to max do
  begin
    Readln(pupil[i].x,pupil[i].y);
    pupil[i].state := false;
    pupil[i].selected := false;
  end;
  CloseFile(input);
  for i := 1 to max do
    begin
      pupil[i].obj := TImage.Create(MainFrame);
      with (pupil[i].obj) do
        begin
          Name := 'Pup' + IntToStr(i);
          Height := 26;
          Width := 26;
          Parent := MainFrame;
          Left := pupil[i].x-3;
          Top := pupil[i].y-3;
          Transparent := true;
          OnClick := MyClick;
        end;
    end;
end;

end.
